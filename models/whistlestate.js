'use strict';

var mongoose = require('mongoose');
var encrypt = require('mongoose-encryption');
var Utils = require('../utils/utils.js');

var WhistleStateSchema = new mongoose.Schema({
  whistle: { type: String, required: true },
  state: { type: String, required: true },
  date: { type: Date, default: Date.now },
  note: { type: String }
});

WhistleStateSchema.plugin(encrypt, {
  encryptionKey: Utils.encKey,
  signingKey: Utils.sigKey,
  decryptPostSave: false,
  excludeFromEncryption: ['whistle']
});

module.exports = mongoose.model('WhistleState', WhistleStateSchema);
