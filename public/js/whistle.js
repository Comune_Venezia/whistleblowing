function whistle(whistleid, filename) {
  var crypto = new Crypto();
  var zip = null;

  function decryptFiles() {
    var oReq = new XMLHttpRequest();
    oReq.open('GET', '/getfile/' + filename, true);
    oReq.responseType = 'arraybuffer';

    oReq.onload = function (oEvent) {
      zip = new JSZip(oReq.response);
      for (file in zip.files) {
        decryptFile(file);
      }
    };

    oReq.send();
  }

  function decryptFile(filename) {
    var armored = zip.file(filename).asText();
    var msg = openpgp.message.readArmored(armored);
    msg = msg.decrypt(crypto.privkey);
    var base64 = msg.getText();
    var blob = crypto.base64toBlob(base64);
    //console.log('file', filename, base64);

    $('<a>')
      .prop('href', window.URL.createObjectURL(blob))
      .prop('download', filename)
      .addClass('mdl-button mdl-js-button mdl-typography--text-uppercase')
      .text(filename)
      .append('<i class="material-icons">file_download</i>')
      .appendTo('#decryptedfiles');
  }

  $('#decrypt').submit(function (e) {
    var msg = $('#encryptedwhistle').text();
    var privkey = $('#privkey').val();
    var passphrase = $('#passphrase').val();
    var layout = document.getElementsByClassName('mdl-layout')[0];
    var modal = document.getElementById('modal');

    try {
      crypto.decryptPrivKey(privkey, passphrase);
      crypto.decryptMsg(msg)
        .then(function(value) {
          var obj = JSON.parse(value);

          for (var p in obj) {
            $('table.decryptedmsg tbody')
              .append('<tr>' +
                      '<td class="mdl-data-table__cell--non-numeric mdl-data-table__cell--wrap">' + p + '</td>' +
                      '<td class="mdl-data-table__cell--non-numeric mdl-data-table__cell--wrap">' + obj[p] + '</td>' +
                      '</tr>');
          }

          $('#decryptedwhistle').text(value);
          $('#decriptsection').removeClass('hide');

          if (filename) {
            decryptFiles();
          }

          $('.mdl-button-decrypt')
            .prop('disabled', true);
        })
        .catch(function(error) {
          $('.error_msg').text('Errore in fase di decripting! Passphrase errata');
          Modal.create(layout, modal);
        });
    } catch(err) {
      $('.error_msg').text('Errore in fase di decripting! Chiave privata non valida');
      Modal.create(layout, modal);
    }

    e.preventDefault();
    return false;
  });

  $('#addnote').submit(function (e) {
    var formdata = new FormData();

    formdata.append('whistle', whistleid);
    formdata.append('state', $('#state').val());
    formdata.append('note', $('#note').val());

    $.ajax({
      type: 'POST',
      url: '/savenote',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false
    })
    .success(function(response) {
      $('table.decrypteduser tbody')
        .append('<tr>' +
                  '<td class="mdl-data-table__cell--non-numeric">' + $('#state').find('option:selected').text() + '</td>' +
                  '<td class="mdl-data-table__cell--non-numeric">' + moment(Date.now()).format('DD/MM/YYYY HH:mm') + '</td>' +
                  '<td class="mdl-data-table__cell--non-numeric mdl-data-table__cell--wrap">' + $('#note').val() + '</td>' +
                '</tr>');
    });

    e.preventDefault();
    return false;
  });
}
